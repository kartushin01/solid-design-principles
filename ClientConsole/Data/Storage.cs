﻿using System;
using System.Collections.Generic;
using System.Text;
using ClientConsole.Entities;

namespace ClientConsole.Data
{
    public  class Storage
    {
        private static Storage instance;
        public  List<Customer> Customers { get; set; } 
        public  List<string> BlacklistedIpAddress { get; set; }
        public Storage()
        {
            
        }

        public static Storage GetInstance()
        {
            if (instance == null)
                instance = new Storage();
            return instance;

        }
    }
}
