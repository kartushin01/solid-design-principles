﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClientConsole.Data;
using ClientConsole.Entities;
using ClientConsole.Interfaces;

namespace ClientConsole.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly Storage _storage;

        public CustomerRepository()
        {
            _storage = Storage.GetInstance();
        }

        public void AddCustomer(Customer customer)
        {
            if (_storage.Customers == null)
            {
                _storage.Customers = new List<Customer>();
            }
            _storage.Customers.Add(customer);
            
        }

        public List<Customer> GetAllCustomer()
        {
            return _storage.Customers; 
        }

        public Customer GetGustomerByEmail(string email)
        {
            return _storage.Customers.FirstOrDefault(c=>c.Email == email);
        }

        public bool CheckIpAddressExists(string ip)
        {
            return _storage.Customers.Any(c => c.IpAddress == ip);
        }
    }
}
