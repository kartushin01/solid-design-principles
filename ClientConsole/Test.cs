﻿using ClientConsole.Entities;
using ClientConsole.Interfaces;
using ClientConsole.Repositories;
using ClientConsole.Services;
using System;
using System.Collections.Generic;

namespace ClientConsole
{
    class Test
    {
        private IStorageSevice _storageService;
        private ICustomerRepository _customerRepository;
        private List<IRegistrationPreCheck> _registrationPreChecks;
        private IRegisterService _registerService;
        public Test()
        {
            _storageService = new StorageService();
            _storageService.SeedStotage();
            _customerRepository = new CustomerRepository();
            _registrationPreChecks = new List<IRegistrationPreCheck>(new List<IRegistrationPreCheck>());
            _registerService = new RegisterService(_customerRepository, _registrationPreChecks);
        }

        public void Run()
        {
            GegisterNewCuctomer();
            PrintAndReadCustomersToCosole();
            TestDisqount(2800);
            PrintAndReadCustomersToCosole();
            DipDemo();
            Console.ReadKey();
        }

        public Customer GenarateCustomer(string name, string email, string phone, string role, string ipAddress)
        {
            return new Customer()
            {
                Id = new Guid(),
                Name = name,
                Email = email,
                Role = role,
                Phone = phone,
                IpAddress = ipAddress
            };
        }

        public void GegisterNewCuctomer()
        {
            Customer customer = GenarateCustomer("tests", "test@mail.com",
                "123456789", "admin", "99.999.99.91");

            IRegistrationServiceConfiguration blackListIpAdress = new RegistrationServiceConfiguration();
            blackListIpAdress.BlacklistedIpAddress.Add("99.999.99.90");

            _registrationPreChecks.Add(new RegistrationPreCheckBlacklistedIpAddress(blackListIpAdress));
            _registrationPreChecks.Add(new RegistrationPreCheckIpAddressExists(_customerRepository));

            _registerService.RegisterCustomer(customer);
        }

        public void TestDisqount(double sum)
        {
            IDisqount disqount = new DisqountSilver();
            Console.WriteLine(disqount.GetDisqount(sum));

            disqount = new DisqountGold();
            Console.WriteLine(disqount.GetDisqount(sum));
        }


        public void PrintAndReadCustomersToCosole()
        {
            CustomersConsole customerConsole = new CustomersConsole(_customerRepository);
            var customers = customerConsole.ReadCustomers();
            customerConsole.PrintCustomers(customers);
        }

        public void DipDemo()
        {
            Product product = new Product(new ConsolepPrinter(), "test1", 3000);
            product.Print();
            product.Printer = new HtmlPrinter();
            product.Print();
        }
    }
}
