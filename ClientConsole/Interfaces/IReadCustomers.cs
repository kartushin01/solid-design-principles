﻿using System;
using System.Collections.Generic;
using System.Text;
using ClientConsole.Entities;


namespace ClientConsole.Interfaces
{
    public interface IReadCustomers
    {
        List<Customer>ReadCustomers();
    }
}
