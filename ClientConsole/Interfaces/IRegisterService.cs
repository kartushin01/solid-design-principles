﻿using System;
using System.Collections.Generic;
using System.Text;
using ClientConsole.Entities;

namespace ClientConsole.Interfaces
{
    public interface IRegisterService
    {
        void RegisterCustomer(Customer customer);
    }
}
