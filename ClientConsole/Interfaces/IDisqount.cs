﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientConsole.Interfaces
{
    public abstract class IDisqount
    {
        public abstract double GetDisqount(double sum);

    }
}
