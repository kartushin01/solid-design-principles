﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientConsole.Interfaces
{
    public interface IRegistrationServiceConfiguration
    {
        List<string> BlacklistedIpAddress { get; }
    }
}
