﻿using System;
using System.Collections.Generic;
using System.Text;
using ClientConsole.Entities;

namespace ClientConsole.Interfaces
{
    public interface ICustomerRepository
    {
        void AddCustomer(Customer customer);
        List<Customer> GetAllCustomer();
        Customer GetGustomerByEmail(string email);

        bool CheckIpAddressExists(string ip);
    }
}
