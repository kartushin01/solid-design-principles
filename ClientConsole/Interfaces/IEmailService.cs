﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientConsole.Interfaces
{
    public interface IEmailService
    {
        void SendEmail(string to, string text);
    }
}
