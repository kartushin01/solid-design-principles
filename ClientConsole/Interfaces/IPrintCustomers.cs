﻿using System;
using System.Collections.Generic;
using System.Text;
using ClientConsole.Entities;

namespace ClientConsole.Interfaces
{
    public interface IPrintCustomers
    {
        void PrintCustomers(List<Customer> customers);
    }
}
