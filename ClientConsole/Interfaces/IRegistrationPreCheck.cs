﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientConsole.Interfaces
{
    public interface IRegistrationPreCheck
    {
        void Check(string value);
    }

}
