﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientConsole.Interfaces
{
    public interface IPrinter
    {
         void Print(string text);
    }
}
