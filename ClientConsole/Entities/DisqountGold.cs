﻿using System;
using System.Collections.Generic;
using System.Text;
using ClientConsole.Interfaces;

namespace ClientConsole.Entities
{
    class DisqountGold : IDisqount
    {
        public override double GetDisqount(double sum)
        {
            return sum * 0.2;
        }
    }
}
