﻿using System;
using System.Collections.Generic;
using System.Text;
using ClientConsole.Interfaces;

namespace ClientConsole.Entities
{
   public class DisqountSilver : IDisqount
    {
        public override double GetDisqount(double sum)
        {
            return sum * 0.1;
        }
    }
}
