﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientConsole.Entities
{
    public class Customer
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string IpAddress { get; set; }
    }
}
