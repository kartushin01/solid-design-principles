﻿using System;
using System.Collections.Generic;
using System.Text;
using ClientConsole.Interfaces;

namespace ClientConsole.Entities
{
    public class Product
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public IPrinter Printer { get; set; }

        public Product(IPrinter printer, string name, decimal price)
        {
            this.Id = new Guid();
            this.Name = name;
            this.Price = price;
            this.Printer = printer;
        }
        public void Print()
        {
            Printer.Print($"{this.Name}: {this.Price}" );
        }
    }
}
