﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using ClientConsole.Data;
using ClientConsole.Entities;
using ClientConsole.Interfaces;
using ClientConsole.Repositories;
using ClientConsole.Services;

namespace ClientConsole
{
    class Program
    {
        static void Main(string[] args)
        {
           Test test = new Test();
           test.Run();
        }
    }
}
