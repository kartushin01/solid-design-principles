﻿using System;
using System.Collections.Generic;
using System.Text;
using ClientConsole.Interfaces;

namespace ClientConsole.Services
{
    public class RegistrationPreCheckBlacklistedIpAddress : IRegistrationPreCheck
    {
        private readonly IRegistrationServiceConfiguration _registrationServiceConfiguration;

        public RegistrationPreCheckBlacklistedIpAddress(IRegistrationServiceConfiguration registrationServiceConfiguration)
        {
            _registrationServiceConfiguration = registrationServiceConfiguration;
        }

        public void Check(string value)
        {
            if (_registrationServiceConfiguration.BlacklistedIpAddress.Contains(value))
                throw new InvalidOperationException("IP Address in black list");
        }
    }
}
