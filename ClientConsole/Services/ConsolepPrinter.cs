﻿using System;
using System.Collections.Generic;
using System.Text;
using ClientConsole.Interfaces;

namespace ClientConsole.Services
{
    class ConsolepPrinter : IPrinter
    {
        public void Print(string text)
        {
            Console.WriteLine("Печать на консоли");
        }
    }
}
