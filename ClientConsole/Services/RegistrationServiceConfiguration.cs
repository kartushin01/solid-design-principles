﻿using System;
using System.Collections.Generic;
using System.Text;
using ClientConsole.Interfaces;

namespace ClientConsole.Services
{
    public class RegistrationServiceConfiguration : IRegistrationServiceConfiguration
    {
        public List<string> BlacklistedIpAddress { get; }

        public RegistrationServiceConfiguration()
        {
       
            BlacklistedIpAddress = new List<string>();
        }
    }
}
