﻿using System;
using System.Collections.Generic;
using System.Text;
using ClientConsole.Interfaces;

namespace ClientConsole.Services
{
    class EmailService : IEmailService
    {
        public void SendEmail(string to, string text)
        {
            Console.WriteLine($"Сообщение отправенно на email: {to}. Текст: {text}");
        }
    }
}
