﻿using System;
using System.Collections.Generic;
using System.Text;
using ClientConsole.Entities;
using ClientConsole.Interfaces;

namespace ClientConsole.Services
{
    class CustomersConsole : IReadCustomers, IPrintCustomers
    {
        private ICustomerRepository _customerRepository;

        public CustomersConsole(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public List<Customer> ReadCustomers()
        {
           return _customerRepository.GetAllCustomer();
        }

        public void PrintCustomers(List<Customer> customers)
        {
            Console.WriteLine($"Все наши клиенты: ({customers.Count} шт.)");

            foreach (var customer in customers)
            {
                Console.WriteLine($"Имя:{customer.Name}, email: {customer.Email}");
            }
        }
    }
}
