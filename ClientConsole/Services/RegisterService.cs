﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClientConsole.Entities;
using ClientConsole.Interfaces;

namespace ClientConsole.Services
{
    public class RegisterService : IRegisterService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IEmailService _emailService;
        private readonly IEnumerable<IRegistrationPreCheck>
            _registrationPreChecks;


        public RegisterService(ICustomerRepository customerRepository, IEnumerable<IRegistrationPreCheck> registrationPreCheck)
        {
            _customerRepository = customerRepository;
            _registrationPreChecks = registrationPreCheck;
            _emailService = new EmailService();
        }

        public void RegisterCustomer(Customer customer)
        {
            _registrationPreChecks.ToList().ForEach(x => x.Check(customer.IpAddress));

            _customerRepository.AddCustomer(customer);
            
            _emailService.SendEmail(customer.Email, "Register Success");

        }

 
    }
}
