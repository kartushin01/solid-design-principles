﻿using System;
using System.Collections.Generic;
using System.Text;
using ClientConsole.Interfaces;

namespace ClientConsole.Services
{
    public class RegistrationPreCheckIpAddressExists : IRegistrationPreCheck
    {
        private ICustomerRepository _customerRepository;

        public RegistrationPreCheckIpAddressExists(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public void Check(string value)
        {
            if (_customerRepository.CheckIpAddressExists(value))
                throw new InvalidOperationException("IP Address Exists");
        }
    }
}
