﻿using System;
using System.Collections.Generic;
using System.Text;
using ClientConsole.Data;
using ClientConsole.Entities;
using ClientConsole.Interfaces;

namespace ClientConsole.Services
{
    public class StorageService : IStorageSevice
    {
        public void SeedStotage()
        {

            Storage storage = Storage.GetInstance();

            if (storage.Customers == null)
            {
                List<Customer> customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = new Guid(),
                        Name = "Иван",
                        Email = "ivan@mail.ru",
                        Phone = "123456789",
                        Role = "Buyer",
                        IpAddress = "99.999.99.88"
                    },
                    new Customer()
                    {
                        Id = new Guid(),
                        Name = "Петя",
                        Email = "petya@mail.ru",
                        Phone = "123456789",
                        Role = "Buyer",
                        IpAddress = "99.999.99.77"
                    },
                    new Customer()
                    {
                        Id = new Guid(),
                        Name = "Маша",
                        Email = "matry@mail.ru",
                        Phone = "123456789",
                        Role = "admin",
                        IpAddress = "99.999.99.99"
                    }
                };

                storage.Customers = customers;
            }
        }
    }
}
